package com.example.bank.component;

import org.springframework.stereotype.Component;

@Component
public class CalculateInterest {
    private double amount;
    private int age;

    public CalculateInterest() {
        this.amount=5000;
        this.age=55;
    }

    public CalculateInterest(double amount, int age) {
        this.amount = amount;
        this.age = age;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
    public void calculate(){
        double interestRate;
        if(age>50){
            interestRate=amount*8/100;
            amount=amount+interestRate;
            System.out.println(amount);
        }
        else{
            interestRate=amount*4/100;
            amount=amount+interestRate;
            System.out.println(amount);
        }
    }
}
