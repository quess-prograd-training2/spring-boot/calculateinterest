package com.example.bank;

import com.example.bank.component.CalculateInterest;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class BankApplication {

	public static void main(String[] args) {
		ConfigurableApplicationContext context = SpringApplication.run(BankApplication.class, args);
		CalculateInterest interestObj = context.getBean(CalculateInterest.class);
		interestObj.calculate();
	}

}
